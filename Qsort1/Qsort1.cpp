// Qsort1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <ctime>
#include <assert.h>
#include <fstream>

std::ofstream out;

template <class It>
const It lower_bound(It front, It end, const typename std::iterator_traits<It>::value_type& value)
{
	for(;front != end && *front <= value; ++front)
		;
	return front;
}
template <class It>
const It upper_bound(It front, It end, const typename std::iterator_traits<It>::value_type& value)
{
	for(;front != end && *front >= value; ++front)
		;
	return front;
}


template <class It>
void qsort(It& begin, It& end)
{
	typedef typename std::iterator_traits<It>::value_type value_type;

	auto swap_it = [] (It& _front, It& _back)
	{
		assert(_front != _back);
		value_type temp = std::move(*_front);
		*_front = std::move(*_back);
		*_back = std::move(temp);
	};

	const size_t size = std::distance(begin, end);

	if(size == 0)
		return;
	else if(size == 2)
	{
		// can't use end in this case because end could be underefencable
		if(*begin > *(begin + 1))
			swap_it(begin, begin + 1);
	}
	else if(size != 1)
	{
		It pivot = begin + size / 2;
		It current_front = begin, current_back = pivot;

		size_t flips = 0;

		auto reset_loop = [&current_front, &current_back, &pivot, begin, end, size, &flips] ()
		{
			pivot = begin + size / 2;
			current_front = begin;
			current_back = pivot;
			++flips;
		};

		// note the pivot position might change. when that happens we need to reset the pivot and the loop.
		// otherwise a value that is actually less than the middle won't get flipped to the left side or vice versa
		for(; current_front < pivot && current_back < end;)
		{
			It lower = lower_bound(current_front, pivot, *pivot);	// possibly a value that needs to flip to the right side.
			It upper = upper_bound(current_back, end, *pivot);		// possibly a value that needs to flip to the left side.
		
			bool haslower = lower != pivot;
			bool hasupper = upper != end;

			if(haslower)
			{
				if(hasupper)
					swap_it(lower, upper);
				else 
				{
					if(std::distance(lower, pivot) == 1)
					{
						// pivot and lower are neighbors.
						swap_it(lower, pivot);

						reset_loop();
					}
					else
					{
						// we have a lower but not a upper.
						// this shifts the pivot left to make room for the lower.

						value_type temp = std::move(*lower);	// move lower to a temp value,
						It left_of_pivot = (pivot - 1);			// cache pivot - 1,
						*lower = std::move(*left_of_pivot);		// move pivot - 1 to lower,
						*left_of_pivot = std::move(*pivot);		// shift pivot left one position,
						*pivot = std::move(temp);				// move the temp value to the pivot position.
					
						reset_loop();
					}

					continue;	// skip the increments below.
				}

				++current_front, ++current_back;
			}
			else 
			{
				if(hasupper)
				{
					if(std::distance(pivot, upper) == 1)
					{
						// pivot and upper are neighbors.
						swap_it(upper, pivot);

						reset_loop();
						continue;	// skip the increments below.
					}
					else
					{
						// no lower, but we have an upper.
						// this shifts the pivot right to make room for the upper.

						value_type temp = std::move(*upper);	// move lower to a temp value,
						It left_of_pivot = (pivot + 1);			// cache pivot + 1,
						*upper = std::move(*left_of_pivot);		// move pivot + 1 to lower,
						*left_of_pivot = std::move(*pivot);		// shift pivot right one position,
						*pivot = std::move(temp);				// move the temp value to the pivot position.

						reset_loop();
						continue;	// skip the increments below.
					}
				}
				else
					// we have neither.
					break;

				++current_front, ++current_back;
			}
		}

		out << "Flips: " << flips << std::endl;

		// now sort the front and back.
		// could be parallized.
		// note pivot
		qsort(begin, pivot);
		qsort(pivot, end);
	}
}

template <class T>
const bool check_sort(const std::vector<T>& vec)
{
	if(vec.size() == 0)
		return true;

	for(auto i = vec.begin(); i + 1 != vec.end();)
	{
		if(*i > *(++i))
			return false;
	}
	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	out.open("test.txt", std::ios_base::trunc);

	srand(std::time(0));

	for(size_t j = 0; j < 10; ++j)
	{
		typedef std::vector<long long> vector_t;
		vector_t test1;
	
		test1.resize(rand() % 25);
		out << "Size: " << test1.size() << std::endl;

		for(size_t i = 0; i < test1.size(); ++i)
		{
			test1[i] = rand();
		}

		vector_t test2(test1.begin(), test1.end());

		qsort(test2.begin(), test2.end());

		if(check_sort(test2))
			out << "qsort passed!" << std::endl;
		else
		{
			out << "qsort FAILED!" << std::endl;
			out << "Test1: ";
			std::copy(test1.begin(), test1.end(), std::ostream_iterator<long long>(out, "\t"));
			out << std::endl;
			out << "Test2: ";
			std::copy(test2.begin(), test2.end(), std::ostream_iterator<long long>(out, "\t"));
			out << std::endl;
		}
	}

	return 0;
}

