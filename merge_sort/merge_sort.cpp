// merge_sort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <ctime>
#include <assert.h>
#include <fstream>

std::ofstream out;

//template <class It>
//void merge(It& first_block_start, It& second_block_start, size_t offset, size_t size)
//{
//	typedef typename std::iterator_traits<It>::value_type value_type;
//
//	It real_first = first_block_start + offset;
//	It real_second = second_block_start + offset;
//
//	if(*real_first > *real_second)
//	{
//		std::swap(*real_first, *real_second);
//	}
//
//	if(size == 2)
//		return;
//
//
//
//		// first should stay, second should move.
//		value_type temp = std::move(*(first_block_start + offset + 1));
//		*(first_block_start + offset + 1) = std::move(*second_block_start);
//		*second_block_start = std::move(temp);
//
//		++offset;
//		if(offset > size)
//			merge(first_block_start, second_block_start, offset, size);
//}

template <class It>
const bool is_end(It current, It end)
{
	return current == end;
}

template <class T>
void merge(typename std::vector<T>::iterator& all_begin, 
		   const typename std::vector<T>::iterator& all_end, 
		   typename std::vector<T>::iterator& left_begin, 
		   const typename std::vector<T>::iterator& left_end, 
		   typename std::vector<T>::iterator& right_begin, 
		   const typename std::vector<T>::iterator& right_end)
{
	assert(!is_end(all_begin, all_end));

	if(is_end(left_begin, left_end))
	{
		if(is_end(right_begin, right_end))
			return;
		else
		{
			*all_begin = std::move(*right_begin);
			++all_begin;
			++right_begin;
			if(is_end(right_begin, right_end) && is_end(all_begin, all_end))
				return;
		}
	}
	else
	{
		if(is_end(right_begin, right_end))
		{
			*all_begin = std::move(*left_begin);
			++all_begin;
			++left_begin;
			if(is_end(left_begin, left_end) && is_end(all_begin, all_end))
				return;
		}
		else 
		{
			// neither are at the end.
			if(*left_begin <= *right_begin)
			{
				*all_begin = std::move(*left_begin);
				++all_begin, ++left_begin;
			}
			else
			{
				*all_begin = std::move(*right_begin);
				++all_begin, ++right_begin;
			}
			if(is_end(left_begin, left_end) && is_end(right_begin, right_end) && is_end(all_begin, all_end))
				return;
		}
	}
	::merge<T>(all_begin, all_end, left_begin, left_end, right_begin, right_end);
}


// in place.
template <class It>
const std::vector<typename std::iterator_traits<It>::value_type> merge_sort(It& begin, It& back)
{
	typedef typename std::iterator_traits<It>::value_type value_type;

	size_t size = std::distance(begin, back);

	std::vector<value_type> all;
	all.resize(size);

	if(size == 1 || size == 0)
	{
		all[0] = *begin;
		return all;		// we're already sorted.
	}

	size_t half = size / 2;
	It front_end = begin + half;
	It back_begin = begin + half;
	std::vector<value_type> left = merge_sort<It>(begin, front_end);
	std::vector<value_type> right = merge_sort<It>(back_begin, back);

	::merge<value_type>(all.begin(),all.end(), left.begin(),left.end(), right.begin(),right.end());

	return all;
}

template <class T>
const bool check_sort(const std::vector<T>& vec)
{
	if(vec.size() == 0)
		return true;

	for(auto i = vec.begin(); i + 1 != vec.end();)
	{
		if(*i > *(++i))
			return false;
	}
	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	out.open("test.txt", std::ios_base::trunc);

	srand(std::time(0));

	for(size_t j = 0; j < 10; ++j)
	{
		typedef std::vector<long long> vector_t;
		vector_t test1;
	
		test1.resize(rand());
		out << "Size: " << test1.size() << std::endl;

		for(size_t i = 0; i < test1.size(); ++i)
		{
			test1[i] = rand();
		}

		const vector_t test2 = merge_sort(test1.begin(), test1.end());

		if(check_sort(test2))
			out << "merge_sort passed!" << std::endl;
		else
		{
			out << "merge_sort FAILED!" << std::endl;
			out << "Test1: ";
			std::copy(test1.begin(), test1.end(), std::ostream_iterator<long long>(out, "\t"));
			out << std::endl;
			out << "Test2: ";
			std::copy(test2.begin(), test2.end(), std::ostream_iterator<long long>(out, "\t"));
			out << std::endl;
		}
	}
	return 0;
}

